package main

import "fmt"

type MyHashSet struct {
	buckets [1000][]int
}

/** Initialize your data structure here. */
func Constructor() MyHashSet {
	hashSet := new(MyHashSet)

	return *hashSet
}

func (this *MyHashSet) getBucketIndex(key int) int {
	return key % 1000
}

func (this *MyHashSet) Add(key int) {
	if this.Contains(key) {
		return
	}

	bucketIndex := this.getBucketIndex(key)
	this.buckets[bucketIndex] = append(this.buckets[bucketIndex], key)
}

func (this *MyHashSet) Remove(key int) {
	bucketIndex := this.getBucketIndex(key)
	for index, keyInBucket := range this.buckets[bucketIndex] {
		if keyInBucket != key {
			continue
		}

		bucket := this.buckets[bucketIndex]

		bucket[index] = bucket[len(bucket)-1]
		this.buckets[bucketIndex] = bucket[:len(bucket)-1]

		break
	}
}

/** Returns true if this set contains the specified element */
func (this *MyHashSet) Contains(key int) bool {
	bucketIndex := this.getBucketIndex(key)
	for _, keyInBucket := range this.buckets[bucketIndex] {
		if keyInBucket == key {
			return true
		}
	}

	return false
}

/**
 * Your MyHashSet object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Add(key);
 * obj.Remove(key);
 * param_3 := obj.Contains(key);
 */
func main() {
	obj := Constructor()
	obj.Add(1)
	obj.Add(2)
	fmt.Println(obj.Contains(1))
	fmt.Println(obj.Contains(3))
	obj.Add(2)
	fmt.Println(obj.Contains(2))
	obj.Remove(2)
	fmt.Println(obj.Contains(2))
}
