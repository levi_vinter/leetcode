package linked_list

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func CreateList(values []int) *ListNode {
	head := &ListNode{}
	tail := head
	for _, val := range values {
		tail.Next = &ListNode{Val: val}
		tail = tail.Next
	}

	return head.Next
}

func PrintList(node *ListNode) {
	var arr []int
	for node != nil {
		arr = append(arr, node.Val)
		node = node.Next
	}

	fmt.Println(arr)
}
