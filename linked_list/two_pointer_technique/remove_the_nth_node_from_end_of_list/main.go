package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func removeNthFromEnd(head *ListNode, n int) *ListNode {
	dummy := ListNode{}
	dummy.Next = head
	pointerA := &dummy
	pointerB := &dummy

	for i := 1; i <= n; i++ {
		pointerA = pointerA.Next
	}

	for pointerA.Next != nil {
		pointerA = pointerA.Next
		pointerB = pointerB.Next
	}

	pointerB.Next = pointerB.Next.Next

	return dummy.Next
}
