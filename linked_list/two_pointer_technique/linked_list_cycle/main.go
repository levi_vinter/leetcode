package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func hasCycle(head *ListNode) bool {
	slowPointer := head
	fastPointer := head
	for i := 0; i < 10001; i++ {
		if fastPointer == nil || fastPointer.Next == nil {
			return false
		}

		fastPointer = fastPointer.Next.Next
		slowPointer = slowPointer.Next

		if fastPointer == slowPointer {
			return true
		}
	}

	return true
}
