package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func getIntersect(head *ListNode) *ListNode {
	slowPointer := head
	fastPointer := head
	for i := 0; i < 10001; i++ {
		if fastPointer == nil || fastPointer.Next == nil {
			return nil
		}

		fastPointer = fastPointer.Next.Next
		slowPointer = slowPointer.Next

		if fastPointer == slowPointer {
			return fastPointer
		}
	}

	return nil
}

func detectCycle(head *ListNode) *ListNode {
	intersectPointer := getIntersect(head)
	if intersectPointer == nil {
		return nil
	}

	cycleStartPointer := head
	for cycleStartPointer != intersectPointer {
		cycleStartPointer = cycleStartPointer.Next
		intersectPointer = intersectPointer.Next
	}

	return cycleStartPointer
}
