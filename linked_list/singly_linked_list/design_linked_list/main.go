package main

import "fmt"

type MyLinkedList struct {
	head   *Node
	length int
}

type Node struct {
	value int
	next  *Node
}

func Constructor() MyLinkedList {
	/** Add sentinel node as head to avoid handling head being nil
	  /* The first node in the list is actually head.next
	*/
	headNode := Node{}

	return MyLinkedList{head: &headNode}
}

/** Get the value of the index-th node in the linked list. If the index is invalid, return -1. */
func (list *MyLinkedList) Get(index int) int {
	if index < 0 || index >= list.length {
		return -1
	}

	currNode := list.head.next
	// Index 0 is just a sentinel node and should not be fetched
	for i := 1; i <= index; i++ {
		currNode = currNode.next
	}

	return currNode.value
}

/** Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. */
func (list *MyLinkedList) AddAtHead(val int) {
	list.AddAtIndex(0, val)
}

/** Append a node of value val to the last element of the linked list. */
func (list *MyLinkedList) AddAtTail(val int) {
	list.AddAtIndex(list.length, val)
}

/** Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. */
func (list *MyLinkedList) AddAtIndex(index int, val int) {
	if index > list.length {
		return
	}

	if index < 0 {
		index = 0
	}

	prevNode := list.head
	for i := 0; i < index; i++ {
		prevNode = prevNode.next
	}

	newNode := Node{value: val}
	newNode.next = prevNode.next
	prevNode.next = &newNode
	list.length++
}

/** Delete the index-th node in the linked list, if the index is valid. */
func (list *MyLinkedList) DeleteAtIndex(index int) {
	if index < 0 || index >= list.length {
		return
	}

	prevNode := list.head
	for i := 0; i < index; i++ {
		prevNode = prevNode.next
	}

	prevNode.next = prevNode.next.next
	list.length--
}

func main() {
	myLinkedList := Constructor()
	myLinkedList.AddAtHead(1)
	myLinkedList.AddAtTail(3)
	myLinkedList.AddAtIndex(1, 2)    // linked list becomes 1->2->3
	fmt.Println(myLinkedList.Get(1)) // return 2
	myLinkedList.DeleteAtIndex(1)    // now the linked list is 1->3
	fmt.Println(myLinkedList.Get(1)) // return 3
}
