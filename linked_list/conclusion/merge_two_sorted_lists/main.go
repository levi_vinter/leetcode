package main

import (
	"gitlab.com/levi_vinter/leetcode/linked_list/linked_list"
)

type ListNode = linked_list.ListNode

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	head := &ListNode{Val: 0, Next: l1}
	pointer1 := head
	pointer2 := l2

	for pointer1.Next != nil {
		if pointer2 != nil && pointer1.Next.Val >= pointer2.Val {
			nextPointer2 := pointer2.Next
			pointer2.Next = pointer1.Next
			pointer1.Next = pointer2
			pointer2 = nextPointer2
		}
		pointer1 = pointer1.Next
	}

	pointer1.Next = pointer2

	return head.Next
}

func main() {
	firstList := linked_list.CreateList([]int{1, 2})
	linked_list.PrintList(firstList)
	secondList := linked_list.CreateList([]int{2, 3, 4})
	linked_list.PrintList(secondList)
	mergedList := mergeTwoLists(firstList, secondList)
	linked_list.PrintList(mergedList)
}
