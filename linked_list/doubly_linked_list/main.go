package main

import "fmt"

type MyLinkedList struct {
	head *DoublyListNode
	tail *DoublyListNode
	size int
}

type DoublyListNode struct {
	value int
	prev  *DoublyListNode
	next  *DoublyListNode
}

func Constructor() MyLinkedList {
	sentinelHeadNode := DoublyListNode{}
	sentinelTailNode := DoublyListNode{}
	sentinelHeadNode.next = &sentinelTailNode
	sentinelTailNode.prev = &sentinelHeadNode

	return MyLinkedList{head: &sentinelHeadNode, tail: &sentinelTailNode}
}

/** Get the value of the index-th node in the linked list. If the index is invalid, return -1. */
func (list *MyLinkedList) Get(index int) int {
	if index < 0 || index >= list.size {
		return -1
	}

	currNode := list.head
	if index+1 < list.size-index {
		for i := 0; i < index+1; i++ {
			currNode = currNode.next
		}
	} else {
		currNode = list.tail
		for i := 0; i < list.size-index; i++ {
			currNode = currNode.prev
		}
	}

	return currNode.value
}

/** Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. */
func (list *MyLinkedList) AddAtHead(val int) {
	prevNode := list.head
	nextNode := list.head.next
	newHead := &DoublyListNode{value: val, next: nextNode, prev: prevNode}
	prevNode.next = newHead
	nextNode.prev = newHead
	list.size++
}

/** Append a node of value val to the last element of the linked list. */
func (list *MyLinkedList) AddAtTail(val int) {
	oldTail := list.tail.prev
	tailSentinel := list.tail
	newTail := &DoublyListNode{value: val, next: tailSentinel, prev: oldTail}
	tailSentinel.prev = newTail
	oldTail.next = newTail
	list.size++
}

/** Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. */
func (list *MyLinkedList) AddAtIndex(index int, val int) {
	if index > list.size {
		return
	}

	if index < 0 {
		index = 0
	}

	var prevNode *DoublyListNode
	var nextNode *DoublyListNode
	if index < list.size-index {
		prevNode = list.head
		for i := 0; i < index; i++ {
			prevNode = prevNode.next
		}
		nextNode = prevNode.next
	} else {
		nextNode = list.tail
		for i := 0; i < list.size-index; i++ {
			nextNode = nextNode.prev
		}
		prevNode = nextNode.prev
	}

	newNode := &DoublyListNode{value: val, next: nextNode, prev: prevNode}
	prevNode.next = newNode
	nextNode.prev = newNode
	list.size++
}

/** Delete the index-th node in the linked list, if the index is valid. */
func (list *MyLinkedList) DeleteAtIndex(index int) {
	if index < 0 || index >= list.size {
		return
	}

	var prevNode *DoublyListNode
	var nextNode *DoublyListNode
	if index < list.size-index {
		prevNode = list.head
		for i := 0; i < index; i++ {
			prevNode = prevNode.next
		}
		nextNode = prevNode.next.next
	} else {
		nextNode = list.tail
		for i := 0; i < list.size-index-1; i++ {
			nextNode = nextNode.prev
		}
		prevNode = nextNode.prev.prev
	}

	prevNode.next = nextNode
	nextNode.prev = prevNode
	list.size--
}

func printList(list *MyLinkedList) {
	fmt.Println("print list")
	currNode := list.head.next
	fmt.Println(list.head.value)
	for currNode.next != nil {
		fmt.Println(currNode.value)
		currNode = currNode.next
	}
	fmt.Println(list.tail.value)
	fmt.Println("print list stop")
}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 */

func main() {
	obj := Constructor()
	obj.AddAtHead(38)
	obj.AddAtTail(66)
	fmt.Println(obj.Get(0))
	obj.DeleteAtIndex(1)
	fmt.Println("Second try")
	fmt.Println(obj.Get(1))
	printList(&obj)
}
