package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func oddEvenList(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	oddPointer := head
	evenPointer := head.Next
	evenHead := head.Next

	for evenPointer != nil && evenPointer.Next != nil {
		oddPointer.Next = evenPointer.Next
		oddPointer = oddPointer.Next
		evenPointer.Next = oddPointer.Next
		evenPointer = evenPointer.Next
	}

	oddPointer.Next = evenHead

	return head
}

func main() {
	sixth := &ListNode{Val: 6}
	fifth := &ListNode{Val: 5, Next: sixth}
	fourth := &ListNode{Val: 4, Next: fifth}
	third := &ListNode{Val: 3, Next: fourth}
	second := &ListNode{Val: 2, Next: third}
	first := &ListNode{Val: 1, Next: second}

	list := oddEvenList(first)
	for list != nil {
		fmt.Println(list.Val)
		list = list.Next
	}
}
