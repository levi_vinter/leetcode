package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	p := reverseList(head.Next)
	head.Next.Next = head
	head.Next = nil

	return p
}

func main() {
	sixth := &ListNode{Val: 6}
	fifth := &ListNode{Val: 5, Next: sixth}
	fourth := &ListNode{Val: 4, Next: fifth}
	third := &ListNode{Val: 3, Next: fourth}
	second := &ListNode{Val: 2, Next: third}
	first := &ListNode{Val: 1, Next: second}

	reversedList := reverseList(first)
	for reversedList != nil {
		fmt.Println(reversedList.Val)
		reversedList = reversedList.Next
	}
}
