package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseList(head *ListNode) *ListNode {
	var prev *ListNode
	curr := head
	for curr != nil {
		nextCurr := curr.Next
		curr.Next = prev
		prev = curr
		curr = nextCurr
	}

	return prev
}

func main() {
	sixth := &ListNode{Val: 6}
	fifth := &ListNode{Val: 5, Next: sixth}
	fourth := &ListNode{Val: 4, Next: fifth}
	third := &ListNode{Val: 3, Next: fourth}
	second := &ListNode{Val: 2, Next: third}
	first := &ListNode{Val: 1, Next: second}

	reversedList := reverseList(first)
	for reversedList != nil {
		fmt.Println(reversedList.Val)
		reversedList = reversedList.Next
	}
}
