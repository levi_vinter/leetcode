package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func isPalindrome(head *ListNode) bool {
	if head == nil {
		return true
	}

	firstHalfEnd := getFirstHalfEnd(head)
	secondHalf := reverseList(firstHalfEnd.Next)

	firstHalf := head
	result := true
	for secondHalf != nil {
		if firstHalf.Val != secondHalf.Val {
			result = false
			break
		}

		firstHalf = firstHalf.Next
		secondHalf = secondHalf.Next
	}

	firstHalfEnd.Next = reverseList(firstHalfEnd.Next)

	return result
}

func getFirstHalfEnd(head *ListNode) *ListNode {
	slowPointer := head
	fastPointer := head

	for fastPointer.Next != nil && fastPointer.Next.Next != nil {
		slowPointer = slowPointer.Next
		fastPointer = fastPointer.Next.Next
	}

	return slowPointer
}

func reverseList(head *ListNode) *ListNode {
	var prev *ListNode

	curr := head
	for curr != nil {
		tempNext := curr.Next
		curr.Next = prev
		prev = curr
		curr = tempNext
	}

	return prev
}

func main() {
	sixth := &ListNode{Val: 1}
	fifth := &ListNode{Val: 2, Next: sixth}
	fourth := &ListNode{Val: 3, Next: fifth}
	third := &ListNode{Val: 3, Next: fourth}
	second := &ListNode{Val: 2, Next: third}
	first := &ListNode{Val: 1, Next: second}

	fmt.Println(isPalindrome(first))
}
