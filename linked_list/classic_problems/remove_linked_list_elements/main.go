package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func removeElements(head *ListNode, val int) *ListNode {
	if val == 0 {
		return head
	}

	dummy := &ListNode{Val: 0, Next: head}
	currNode := dummy
	for currNode.Next != nil {
		if currNode.Next.Val == val {
			currNode.Next = currNode.Next.Next
			continue
		}
		currNode = currNode.Next
	}

	return dummy.Next
}

func main() {
	sixth := &ListNode{Val: 6}
	fifth := &ListNode{Val: 5, Next: sixth}
	fourth := &ListNode{Val: 4, Next: fifth}
	third := &ListNode{Val: 3, Next: fourth}
	second := &ListNode{Val: 2, Next: third}
	first := &ListNode{Val: 1, Next: second}
	val := 1

	list := removeElements(first, val)
	for list != nil {
		fmt.Println(list.Val)
		list = list.Next
	}
}
