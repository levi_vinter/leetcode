package main

import (
	"fmt"

	"gitlab.com/levi_vinter/leetcode/binary_tree/binary_tree"
)

type TreeNode = binary_tree.TreeNode

// func preorderTraversal(root *TreeNode) []int {
// 	if root == nil {
// 		return []int{}
// 	}
// 	output := []int{root.Val}
// 	if root.Left != nil {
// 		output = append(output, preorderTraversal(root.Left)...)
// 	}
// 	if root.Right != nil {
// 		output = append(output, preorderTraversal(root.Right)...)
// 	}

// 	return output
// }
func preorderTraversal(root *TreeNode) []int {
	result := []int{}
	stack := []*TreeNode{}
	if root != nil {
		stack = append(stack, root)
	}

	var current *TreeNode
	for len(stack) != 0 {
		current = stack[len(stack)-1]
		result = append(result, current.Val)
		stack = stack[:len(stack)-1]
		if current.Right != nil {
			stack = append(stack, current.Right)
		}
		if current.Left != nil {
			stack = append(stack, current.Left)
		}
	}

	return result
}

func main() {
	values := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	root := binary_tree.CreateTree(values)
	result := preorderTraversal(root)
	fmt.Println(result)
}
