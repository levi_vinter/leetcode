package main

import (
	"fmt"

	"gitlab.com/levi_vinter/leetcode/binary_tree/binary_tree"
)

type TreeNode = binary_tree.TreeNode

// func postorderTraversal(root *TreeNode) []int {
// 	output := []int{}
// 	if root == nil {
// 		return output
// 	}
// 	if root.Left != nil {
// 		output = append(output, postorderTraversal(root.Left)...)
// 	}
// 	if root.Right != nil {
// 		output = append(output, postorderTraversal(root.Right)...)
// 	}
// 	output = append(output, root.Val)

// 	return output
// }

func postorderTraversal(root *TreeNode) []int {
	output := []int{}

	if root == nil {
		return output
	}

	stack := []*TreeNode{root}
	var node *TreeNode
	for len(stack) != 0 {
		node, stack = stack[len(stack)-1], stack[:len(stack)-1]
		output = append(output, 0)
		copy(output[1:], output)
		output[0] = node.Val
		if node.Left != nil {
			stack = append(stack, node.Left)
		}
		if node.Right != nil {
			stack = append(stack, node.Right)
		}
	}

	return output
}

func main() {
	values := []int{9, 5, 1, 4, 2, 3, 8, 7, 6}
	root := binary_tree.CreateTree(values)
	result := postorderTraversal(root)
	fmt.Println(result)
}
