package main

import (
	"fmt"

	"gitlab.com/levi_vinter/leetcode/binary_tree/binary_tree"
)

type TreeNode = binary_tree.TreeNode

func levelOrder(root *TreeNode) [][]int {
	output := [][]int{}
	if root == nil {
		return output
	}
	queue := []*TreeNode{root}
	var node *TreeNode
	var level []int

	for len(queue) != 0 {
		queueLength := len(queue)
		level = []int{}
		for i := 0; i < queueLength; i++ {
			node, queue = queue[len(queue)-1], queue[:len(queue)-1]
			if node.Left != nil {
				queue = append(queue, nil)
				copy(queue[1:], queue)
				queue[0] = node.Left
			}
			if node.Right != nil {
				queue = append(queue, nil)
				copy(queue[1:], queue)
				queue[0] = node.Right
			}
			level = append(level, node.Val)
		}
		output = append(output, level)
	}

	return output
}

func main() {
	values := []int{1, 2, 4, 5, 7, 8, 3, 6, 9}
	root := binary_tree.CreateTree(values)
	result := levelOrder(root)
	fmt.Println(result)
}
