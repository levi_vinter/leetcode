package main

import (
	"fmt"

	"gitlab.com/levi_vinter/leetcode/binary_tree/binary_tree"
)

type TreeNode = binary_tree.TreeNode

// func inorderTraversal(root *TreeNode) []int {
// 	output := []int{}
// 	if root == nil {
// 		return output
// 	}
// 	if root.Left != nil {
// 		output = append(output, inorderTraversal(root.Left)...)
// 	}
// 	output = append(output, root.Val)
// 	if root.Right != nil {
// 		output = append(output, inorderTraversal(root.Right)...)
// 	}

// 	return output
// }
func inorderTraversal(root *TreeNode) []int {
	result := []int{}
	var stack []*TreeNode
	current := root
	for current != nil || len(stack) != 0 {
		for current != nil {
			stack = append(stack, current)
			current = current.Left
		}
		current = stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		result = append(result, current.Val)
		current = current.Right
	}

	return result
}

func main() {
	values := []int{6, 2, 1, 4, 3, 5, 7, 9, 8}
	root := binary_tree.CreateTree(values)
	result := inorderTraversal(root)
	fmt.Println(result)
}
