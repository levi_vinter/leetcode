package main

import (
	"fmt"
	"math"

	"gitlab.com/levi_vinter/leetcode/binary_tree/binary_tree"
)

type TreeNode = binary_tree.TreeNode

func maxDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}

	leftValue := maxDepth(root.Left)
	rightValue := maxDepth(root.Right)

	return int(math.Max(float64(leftValue), float64(rightValue))) + 1
}

func main() {
	values := []int{6, 2, 1, 4, 3, 5, 7, 9, 8}
	root := binary_tree.CreateTree(values)
	result := maxDepth(root)
	fmt.Println(result)
}
