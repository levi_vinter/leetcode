package binary_tree

type TreeNode struct {
	Left  *TreeNode
	Right *TreeNode
	Val   int
}

func CreateTree(values []int) *TreeNode {
	rightRightLeft := TreeNode{Val: values[8]}
	rightRight := TreeNode{Val: values[7], Left: &rightRightLeft}
	right := TreeNode{Val: values[6], Right: &rightRight}
	leftRightRight := TreeNode{Val: values[5]}
	leftRightLeft := TreeNode{Val: values[4]}
	leftRight := TreeNode{Val: values[3], Left: &leftRightLeft, Right: &leftRightRight}
	leftLeft := TreeNode{Val: values[2]}
	left := TreeNode{Val: values[1], Left: &leftLeft, Right: &leftRight}
	root := TreeNode{Val: values[0], Left: &left, Right: &right}

	return &root
}
