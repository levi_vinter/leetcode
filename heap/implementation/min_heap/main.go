package main

import (
	"fmt"
	"math"
)

type MinHeap struct {
	heap     []int
	heapSize int
	realSize int
}

func New(heapSize int) MinHeap {
	// To better track the indices of the binary tree,
	// we will not use the 0-th element in the array
	// You can fill it with any value
	heapArray := make([]int, heapSize+1)
	minHeap := MinHeap{heap: heapArray, heapSize: heapSize}

	return minHeap
}

func (minHeap *MinHeap) add(value int) {
	if minHeap.realSize >= minHeap.heapSize {
		fmt.Println("Added too many elements")
		return
	}

	minHeap.realSize++

	index := minHeap.realSize

	minHeap.heap[index] = value

	// Parent node of the newly added element
	// Note if we use an array to represent the complete binary tree
	// and store the root node at index 1
	// index of the parent node of any node is [index of the node / 2]
	// index of the left child node is [index of the node * 2]
	// index of the right child node is [index of the node * 2 + 1]
	parentIndex := index / 2

	for minHeap.heap[index] < minHeap.heap[parentIndex] && index > 1 {
		minHeap.heap[index] = minHeap.heap[parentIndex]
		minHeap.heap[parentIndex] = value
		index = parentIndex
		parentIndex = index / 2
	}
}

func (minHeap *MinHeap) peek() int {
	if minHeap.realSize == 1 {
		fmt.Println("Don't have any element")
		return math.MaxInt
	}
	return minHeap.heap[1]
}

func (minHeap *MinHeap) pop() int {
	if minHeap.realSize == 1 {
		fmt.Println("Don't have any element")
		return math.MaxInt
	}

	topElement := minHeap.heap[1]

	minHeap.heap[1] = minHeap.heap[minHeap.realSize]
	minHeap.realSize--

	index := 1
	for index <= minHeap.realSize/2 {
		leftNodeIndex := index * 2
		rightNodeIndex := index*2 + 1

		// If the deleted element is larger than the left or right child
		// its value needs to be exchanged with the smaller value
		// of the left and right child
		if minHeap.heap[index] > minHeap.heap[leftNodeIndex] && minHeap.heap[index] > minHeap.heap[rightNodeIndex] {
			break
		}

		indexToExchange := leftNodeIndex
		if minHeap.heap[rightNodeIndex] < minHeap.heap[leftNodeIndex] {
			indexToExchange = rightNodeIndex
		}

		temp := minHeap.heap[index]
		minHeap.heap[index] = minHeap.heap[indexToExchange]
		minHeap.heap[indexToExchange] = temp

		index = indexToExchange
	}

	return topElement
}

func (minHeap *MinHeap) toString() string {
	return fmt.Sprintf("%v", minHeap.heap[1:minHeap.realSize+1])
}

func (minHeap *MinHeap) size() int {
	return minHeap.realSize
}

func main() {
	minHeap := New(3)

	minHeap.add(3)
	minHeap.add(1)
	minHeap.add(2)

	fmt.Println(minHeap.toString())
	fmt.Println(minHeap.peek())
	fmt.Println(minHeap.pop())
	fmt.Println(minHeap.toString())

	minHeap.add(4)
	minHeap.add(5)
	fmt.Println(minHeap.toString())
}
