package main

import (
	"fmt"
	"math"
)

type MaxHeap struct {
	heap     []int
	heapSize int
	realSize int
}

func NewMaxHeap(heapSize int) MaxHeap {
	// To better track the indices of the binary tree,
	// we will not use the 0-th element in the array
	// You can fill it with any value
	heap := make([]int, heapSize+1)
	maxHeap := MaxHeap{heap: heap, heapSize: heapSize}

	return maxHeap
}

func (maxHeap *MaxHeap) add(value int) {
	if maxHeap.realSize >= maxHeap.heapSize {
		fmt.Println("Added too many elements")
		return
	}

	maxHeap.realSize++

	index := maxHeap.realSize

	maxHeap.heap[index] = value

	// Parent node of the newly added element
	// Note if we use an array to represent the complete binary tree
	// and store the root node at index 1
	// index of the parent node of any node is [index of the node / 2]
	// index of the left child node is [index of the node * 2]
	// index of the right child node is [index of the node * 2 + 1]
	parentIndex := index / 2

	for maxHeap.heap[index] > maxHeap.heap[parentIndex] && index > 1 {
		maxHeap.heap[index] = maxHeap.heap[parentIndex]
		maxHeap.heap[parentIndex] = value
		index = parentIndex
		parentIndex = index / 2
	}
}

func (maxHeap *MaxHeap) peek() int {
	if maxHeap.realSize < 1 {
		fmt.Println("No element")
		return math.MinInt
	}
	return maxHeap.heap[1]
}

func (maxHeap *MaxHeap) pop() int {
	if maxHeap.realSize < 1 {
		fmt.Println("Don't have any element")
		return math.MinInt
	}

	topElement := maxHeap.heap[1]

	maxHeap.heap[1] = maxHeap.heap[maxHeap.realSize]
	maxHeap.realSize--

	index := 1
	for index <= maxHeap.realSize/2 {
		leftNodeIndex := index * 2
		rightNodeIndex := index*2 + 1

		// If the deleted element is larger than the left or right child
		// its value needs to be exchanged with the smaller value
		// of the left and right child
		if maxHeap.heap[index] < maxHeap.heap[leftNodeIndex] && maxHeap.heap[index] < maxHeap.heap[rightNodeIndex] {
			break
		}

		indexToExchange := leftNodeIndex
		if maxHeap.heap[rightNodeIndex] > maxHeap.heap[leftNodeIndex] {
			indexToExchange = rightNodeIndex
		}

		temp := maxHeap.heap[index]
		maxHeap.heap[index] = maxHeap.heap[indexToExchange]
		maxHeap.heap[indexToExchange] = temp

		index = indexToExchange
	}

	return topElement
}

func (maxHeap *MaxHeap) toString() string {
	return fmt.Sprintf("%v", maxHeap.heap[1:maxHeap.realSize+1])
}

func (maxHeap *MaxHeap) size() int {
	return maxHeap.realSize
}

func main() {
	maxHeap := NewMaxHeap(5)

	maxHeap.add(1)
	maxHeap.add(2)
	maxHeap.add(3)
	fmt.Println(maxHeap.toString())

	fmt.Println(maxHeap.peek())
	fmt.Println(maxHeap.pop())
	fmt.Println(maxHeap.pop())
	fmt.Println(maxHeap.pop())
	fmt.Println(maxHeap.toString())

	maxHeap.add(4)
	maxHeap.add(5)
	fmt.Println(maxHeap.toString())
}
